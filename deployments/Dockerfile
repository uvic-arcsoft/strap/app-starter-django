# Stage 1: Build stage
FROM python:3.12.3-alpine AS builder

# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install build dependencies
RUN apk update \
    && apk add --no-cache --virtual .build-deps postgresql-dev gcc python3-dev musl-dev git

# install dependencies
RUN pip install --upgrade pip
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Install django_editablecontent app
RUN pip install --no-cache-dir django-editablecontent --index-url https://gitlab.com/api/v4/projects/55582044/packages/pypi/simple

# copy project
COPY ./app_starter .

# Stage 2: Final stage
FROM python:3.12.3-alpine

# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install runtime dependencies
RUN apk update && apk add --no-cache postgresql-libs

# copy python dependencies and application code from the build stage
COPY --from=builder /usr/local/lib/python3.12/site-packages /usr/local/lib/python3.12/site-packages
COPY --from=builder /usr/local/bin /usr/local/bin
COPY --from=builder /usr/src/app /usr/src/app

# Create a new user and set this user (id 1000) as the owner of the work directory
RUN adduser -D -u 1000 django \
    && chown -R django /usr/src/app \
    && chown -R django /tmp

USER django

# Run migration and start WSGI server
ENTRYPOINT ["sh", "./start.sh"]
