# Basic containerization for Django App Starter
This basic app starter includes 2 containers: the main Django app container and an Nginx container to serve static files. Please follow the steps below to build and start the containers.

## Create neccessary environment variables
This was also mentioned in `DEVELOPMENT.md`, make sure you have an environment variable file at `app_starter/app_starter/.env`:

    SECRET_KEY=yoursupersecretkey
    DEBUG=True
    ALLOWED_HOSTS=domain.example.org
    CSRF_TRUSTED_ORIGINS=http://domain.example.org:8080

## Move static files into the right directory
From the root directory, simply run:

    python3 app_starter/manage.py collectstatic --no-input

to copy all static files into `/staticfiles`. When building Nginx container, there will be a step to copy all static files from this directory into the container. Therefore, it is necessary to have all static files moved into this directory.

## Build and run the containers
Build the main Django app container:

    docker build -f deployments/Dockerfile -t app_starter:latest .

Build the Nginx container:

    docker build -f deployments/nginx/Dockerfile -t app_starter_nginx:latest .

Stand the containers up in a detached mode:

    docker-compose -f deployments/docker-compose.yml up -d

Visit the app at `<your_ip>:8080`

Stop the containers:

    docker-compose -f deployments/docker-compose.yml down