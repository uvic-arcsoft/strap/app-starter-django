# pylint:
import os
import subprocess
import re

with open("CHANGELOG.md", 'w', encoding='utf-8') as changelog:
    with subprocess.Popen(
        "git tag -n5",
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        shell=True
    ) as process:
        process.wait(timeout=120)
        output = process.communicate()[0].decode()
        for line in output.splitlines():
            [version, description] = re.split(r'\s{4,}', line)
            changelog.write(f'### {version} - {description}\n')

print("Finish updating changelog")

# add CHANGELOG.md to git
os.system('git add CHANGELOG.md')
os.system("git commit -m 'Update CHANGELOG.md'")
