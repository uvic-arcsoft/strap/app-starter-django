# pylint:
from django_editablecontent.views import EditableContentView

# User Page: Authenticated users can visit the user page (either as a normal user or as admin)
# View class: https://docs.djangoproject.com/en/4.1/ref/class-based-views/base/
class HomePage(EditableContentView):
    """ View class for normal user dashboard """
    template_name = "normal_users/dashboard.html"
    editable_content_names = ['dashboard_content']

    def get_context_data(self, **kwargs):
        """ Set context data for the render template """
        context = super().get_context_data(**kwargs)
        return context
