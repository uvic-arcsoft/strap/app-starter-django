# pylint:
from django.urls import path
from . import views

urlpatterns = [
    path('', views.AdminHomePage.as_view(), name="admin_home_page"),
]
