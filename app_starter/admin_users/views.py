# pylint:
from mixins import AdminRequiredMixin
from django_editablecontent.views import EditableContentView

# Create your views here.
# Admin Page: Only authenticated admin users can visit this page
# View class: https://docs.djangoproject.com/en/4.1/ref/class-based-views/base/
class AdminHomePage(AdminRequiredMixin, EditableContentView):
    """ View class for admin dashboard """
    template_name = "admin_users/dashboard.html"
    editable_content_names = ['admin_dashboard_content']

    def get_context_data(self, **kwargs):
        """ Set context data for the render template """
        context = super().get_context_data(**kwargs)
        return context
