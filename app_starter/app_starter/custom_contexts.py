# pylint:
from django.conf import settings

# Set up configurations specified in settings.py
# W0613 is disabled as functions that populate global context in Django
# needs to take request as an argument
# pylint: disable-next=W0613
def project_setup(request):
    project_config = settings.CONFIG
    return {
        'project_title': project_config['PROJECT_TITLE'],
        'project_version': project_config['PROJECT_VERSION'],
        'project_homepage': project_config['PROJECT_HOMEPAGE'],
        'about_content': project_config['ABOUT_CONTENT']
    }
