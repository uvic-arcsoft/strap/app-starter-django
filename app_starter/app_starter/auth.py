# pylint:
import jwt

from django.contrib.auth.models import User
from django.contrib.auth import login, logout
from django.conf import settings
from django.shortcuts import redirect
from django.views import View

from django_editablecontent.views import EditableContentView
from exceptions import BadConfig

# Login Page: Unauthenticated users and first-time visiters will have to
# get through this view
# View class: https://docs.djangoproject.com/en/4.1/ref/class-based-views/base/
class LoginView(EditableContentView):
    template_name = "login.html"

    def dispatch(self, request, *args, **kwargs):
        username = None

        # For testing, can edit 'AUTHX_USER_OVERRIDE' to replace the HTTP request headers in settings.py
        if settings.DEBUG and settings.CONFIG.get('AUTHX_USER_OVERRIDE'):
            username = settings.CONFIG.get('AUTHX_USER_OVERRIDE')

        # Get username from 'X-Forwarded-User' header
        else:
            header = settings.CONFIG['AUTHX_HTTP_HEADER_USER']
            if header is None:
                raise BadConfig('Missing definition for HTTP request header container user ID')

            username = request.headers.get(header, None)

        # Authenticate user with Django
        if username:
            try:
                user = User.objects.get(username=username)
            except User.DoesNotExist:
                user = User.objects.create_user(username=username)

            # Populate user profile with ID Token received from authentication service
            id_token = request.headers.get(settings.CONFIG.get('AUTHX_HTTP_HEADER_ID_TOKEN', None), None)
            if id_token:
                id_token = id_token.replace("Bearer ", "")
                user_info = jwt.decode(id_token, options={"verify_signature": False})
                user.first_name = user_info.get('given_name', '')
                user.last_name = user_info.get('family_name', '')
                user.email = user_info.get('email', '')
                user.save()

            login(request, user)

            # Redirect normal user to the homepage
            if not user.is_staff:
                return redirect("/")

            # Redirect admin user to the admin page
            return redirect("/admin")

        return super().dispatch(request, *args, **kwargs)

# Logging users out
# View class: https://docs.djangoproject.com/en/4.1/ref/class-based-views/base/
class LogoutView(View):
    # Disable arguments-differ as the missing argumentes are unused
    # pylint: disable=arguments-differ
    def dispatch(self, request):
        if request.session.get('user'):
            del request.session['user']
        # Log admin users out
        if request.session.get('is_admin'):
            del request.session['is_admin']

        # For TESTING ONLY
        if settings.DEBUG:
            settings.CONFIG['AUTHX_USER_OVERRIDE'] = ""

        logout(request)

        return redirect("/login")
