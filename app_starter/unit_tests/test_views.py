# pylint:
from django.test import TestCase, Client
from django.contrib.auth.models import User

class ViewTestCase(TestCase):
    """Views test case class"""
    def setUp(self):
        # Create an user with admin rights in the testing database
        self.admin = User.objects.create(username="admin@example.org", password="testingadmin", is_staff=True)

        # Fake client with admin rights
        self.client = Client()
        self.client.force_login(self.admin)

    def test_get_normal_user_dashboard(self):
        """Test getting normal user dashboard"""
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)
