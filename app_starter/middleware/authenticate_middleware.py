# pylint:
from django.conf import settings
from django.shortcuts import redirect

# This paths are unaffected by the middleware
LOGIN_PATH = "/login/"
ADMIN_PATH = "/admin-django/"

class StarterAuthenticateMiddleware():
    """
    This middleware checks if user is authenticated before every view.
    An unauthenticated user will be redirected to the login page
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        # redirect user to login page if user is not authenticated
        if (request.path != LOGIN_PATH
            and ADMIN_PATH not in request.path
            and not settings.EDITABLECONTENT_TESTING
            and not request.user.is_authenticated):
            return redirect('/login/')

        response = self.get_response(request)

        return response
