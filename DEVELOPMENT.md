# Development and testing

## Getting started

### Create a virtual environment

    $ python3 -m venv venv
    $ . venv/bin/activate
    $ pip install --upgrade pip     # Upgrade pip
    $ pip install -r requirements.txt   # for getting application requirements
    $ pip install -r tests/requirements.txt   # ...testing requirements
    $ pip install django-editablecontent --index-url https://gitlab.com/api/v4/projects/55582044/packages/pypi/simple           # Install django_editablecontent app

### Retrieving submodule(s)

There following submodules are called from this repository:

- the `intest` module, which handles inspection testing (linting)
- the `app_starter` module, which provides basic routings and an Sqlite database of a Django application

The intest module needs to be available for testing, but not for execution and
should not be included in deployments.

This should pull the submodule(s) down:

    git submodule init
    git submodule sync
    git submodule update --init --recursive

Though there is almost certainly a more compact syntax available to carry this
out.

### Retrieving static resources

Not applicable in the template.  Static resources are those which do not
change, or at least do not as part of this project, and can be accessed from
an external website, so that when this application is packaged as a container,
it is not necessary to include these resources.  In addition, they don't need
to be stored with the rest of the project source.

Examples:
- external JavaScript libraries
- icon libraries
- third-party images

### Start up the application
Firstly, from the root directotry, run:

    $ touch app_starter/app_starter/.env   # Create a file for environment variables
    $ python3 app_starter/manage.py shell -c 'from django.core.management import utils; print(utils.get_random_secret_key())' # Generate a Django SECRET_KEY
    $ go to the app_starter/app_starter/.env file, add two lines:
    1. SECRET_KEY=<output_from_the_command_above>
    2. DEBUG=True
    3. ALLOWED_HOSTS=domain.example.org
    4. CSRF_TRUSTED_ORIGINS=http://domain.example.org:8080

The following command line tells Django what app to run, what environment to use, and what port to use:

    $ python3 app_starter/manage.py runserver 127.0.0.1:4001
    Watching for file changes with StatReloader
    Performing system checks...

    System check identified no issues (0 silenced).
    September 08, 2022 - 10:01:46
    Django version 4.1.1, using settings 'app_starter.settings'
    Starting development server at http://127.0.0.1:4001/
    Quit the server with CTRL-BREAK.

### Create basic users
From root directory, run:

    $ python3 app_starter/manage.py createusers
    Successfully created a new user with username user@example.org
    Successfully created a new user with username user1@example.org
    Successfully created a new user with username admin@example.org
    Successfully created a new user with username admin1@example.org

This commands creates 4 users with the emails shown above. To change emails that are created, change `CONFIG['AUTHZ_USERS']` and `CONFIG['AUTHZ_ADMINS']` in `settings.py`.

You are logged in as **admin@example.org** by default. If you want to be logged in as a different user, change `CONFIG['AUTHX_USER_OVERRIDE']`.

### Run unit tests and coverage test
From root directory, run:

    $ python3 app_starter/manage.py test app_starter/unit_tests

This will give you both the **test results** and **coverage result** in the standard output.
