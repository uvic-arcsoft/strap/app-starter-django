# STRAP application template for Django

Based on [the Flask application template](https://gitlab.com/dleske/oft).
The belows are included in this application

## Basic routing
### **Login Page** ("/login/"):
- If user is not authenticated, they will see the **Login page**
- If user is authenticated and is a normal user, they will be redirected to the **User Page** ("/")
- If user is authenticated and is an admin user, they will be redirected to the **Admin Page** ("/admin/")

### **User Page** ("/"):
- If user is not authenticated, they will be redirected to the **Login Page** ("/login/") 
- If user is authenticated (either as a normal user or an admin user), they will see the **User Page** 

### **Admin Page** ("/admin/"):
- If user is not authenticated, they will be redirected to the **Login page** ("/login/") 
- If user is authenticated and is a normal user, they will be redirected to the **User Page** ("/")
- If user is authenticated and is an admin user, they will see **Admin Page** ("/admin/") 

To test, change `CONFIG['AUTHX_USER_OVERRIDE']` in `app_starter/app_starter/settings.py` to an user you create in the database. Default is currently `admin@example.org`.

## Testing
### Linting test
Linting test is setup in the CI so everytime you push code to a Git repo lint test will be automatically executed. To add a **.py** file to lint test, add `#pylint:` to the top of the file. To disable a certain error, add `#pylint: disable=<error_code>`.

### Unit test
Unit tests are setup locally and in the CI. The tests are put in the directory `app_starter/unit_tests`. To run the tests, from root directory, run:

    $ ./tests/coveragetest

To add a new file for test, simply add `test_<name>.py` to this directory. For more information about unit test in Django, visit [running and writing tests in Django](https://docs.djangoproject.com/en/4.1/topics/testing/overview/).

### Coverage test
When you run unit tests, you will receive report for coverage test too (both locally and in the CI).

## Front end tools
**Bootstrap 5.2.2** and **Jquery 3.6.1** are added using CDN. If you wish to remove or change version, modify the import lines of code in `app_starter/templates/base.html`

## Makefile for development
Note that **app_starter/version.py** and **CHANGELOG.md** are updated using Makefile.
- For **app_starter/version.py**, visit `app_starter/app_starter/settings.py`, make changes to `CONFIG['PROJECT_VERSION']` and `CONFIG['VERSION_DESCRIPTION']`.
- For **CHANGELOG.md**, run `git add` and `git commit` on all changed (new, modified, deleted, renamed, etc) files.

After doing the above, simply run `make`, **app_starter/version.py** and **CHANGELOG.md** are updated, added and committed to `git`.