# pylint:
import subprocess
import os
import sys

# Get the current version set in settings.py
command = [
    'python3', 'app_starter/manage.py', 'shell', '-c',
    "from django.conf import settings; print(settings.CONFIG['PROJECT_VERSION'])"
]
output = subprocess.check_output(command)
version = output.decode('UTF-8').rstrip()
command = [
    'python3', 'app_starter/manage.py', 'shell', '-c',
    "from django.conf import settings; print(settings.CONFIG['VERSION_DESCRIPTION'])"
]
output = subprocess.check_output(command)
version_description = output.decode('UTF-8').rstrip()

print('Selected version: ' + version)
print('Version description: ' + version_description)

# Get the current git tags, if the tag specified in CONFIG['PROJECT_VERSION']
# already exists, this script should fail and stop being executed
tags = subprocess.check_output(['git', 'tag']).decode('UTF-8').split('\n')
if version in tags:
    print('This version already exists, please select a different one')
    sys.exit()

# Run git tag with tag as version and annotated message as the version description
os.system(f"git tag -a {version} -m '{version_description}'")
print('Current tags:')
os.system('git tag')

# Write the new version to app_starter/version.py
with open('app_starter/version.py', 'w', encoding='utf-8') as version_file:
    version_file.write('# pylint:\n')
    version_file.write('# This file keep tracks of the current app version\n')
    version_file.write(f"verion = '{version}'\n")
    version_file.write(f"verion_description = '{version_description}'\n")
    version_file.close()

# git add and commit app_starter/version.py
os.system('git add app_starter/version.py')
os.system("git commit -m 'Update version for the application'")
